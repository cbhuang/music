======================================================
Bill's Guitar Compositions
======================================================

Tabs are revised just like codes, isn't it? Therefore why not save them on Git repos.

License
==========

All the original contents of mine are `CC-BY <https://creativecommons.org/licenses/by/4.0>`_-licensed
unless otherwise mentioned.


Listen
===================

- `Simulated plays <https://www.youtube.com/playlist?list=PLsHELEsj9VT6T0Mkll5baP2vDqYu825nS>`_ using Realistic Sound Engine (RSE) in `Guitar Pro <https://www.guitar-pro.com>`_ 6.
- Real performance will be done gradually after September 2020.
